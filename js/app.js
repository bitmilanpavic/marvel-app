var app = {

  // Define API KEYS for our Marvel App.
  API_KEY_PUBLIC: '0b80a07b4dbdc73eaf874a93e911f33c',
  API_KEY_PRIVATE: '7e973a4c88190433739046fe004a833469edf453',

  // Final KEY for authentication comes in format ts(timestamp)=1 + api_key=public + hash=md5(ts+privateKey+publicKey).
  API_KEY_FINAL:'&ts=1&apikey=0b80a07b4dbdc73eaf874a93e911f33c&hash=b17b64820076cdfd01380a565f37ce54',

  // End point where our app is sending request
  API_END_POINT: 'http://gateway.marvel.com/v1/public/',

  // Define starting app values
  search_field : document.querySelector('.search_field'),
  search_value: '',
  marvel_results: document.querySelector('.marvel-results'),
  offset: 0,
  limit:13,
  timeout: null,
  timeout_delay: 500,
  page_number: 1,


  ///
  // Define app init(bootstrap) function
  ///
  init: function(){
    // Clear input value on refresh(Edge fix)
    window.onload = function() {app.search_field.value = '';}

    // List all bokmarks
    app.list_bookmarks();

    // Add keyup event for search field
    app.search_field.addEventListener('keyup', app.search);
  },


  ///
  // Define app ajax function
  // This function is sending request to our API server
  ///
  ajax_call: function(callback){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
          callback( JSON.parse(xmlhttp.responseText) );
      }
    }
    xmlhttp.open("GET", app.API_END_POINT + "characters?nameStartsWith=" + app.search_value + "&limit=" + app.limit + "&offset="+ app.offset + app.API_KEY_FINAL, true);
    xmlhttp.send();
  },


  ///
  // Define app search function, here we handle search results
  ///
  search: function(){

    // Reset offset and page numer if we update search field
    if(app.search_value != this.value){
      app.page_number = 1;
      app.offset = 0;
    }

    // Store search value
    app.search_value = this.value;

    // Delay keyup event for better user experiance and to prevent spaming API
    clearTimeout(app.timeout);
    app.timeout = setTimeout(function () {

      // If search field is not empty search characters
      if(app.search_value != ''){

        // Call ajax function to fetch data
        app.ajax_call(function(response){
          var results = response.data.results,
          result_html = '';

          // Get bookmarks
          var bookmarked_characters = JSON.parse(localStorage.getItem('___bookmarked_characters'));
          bookmarked_characters = (bookmarked_characters == null)? [] : bookmarked_characters;

          // Print search results if we have them
          if(results.length > 0 ){
            var total_results = (results.length > 0)? results.length: 2;

            // Loop true results
            for (var i = 0; i < total_results - 1; i++) {

              // Get character name image and id
              var name = results[i].name,
              thumbnail = results[i].thumbnail,
              id = results[i].id;

              // Search for current character in our bookmarks
              bookmarked_character = bookmarked_characters.filter(function(e){
                return e.id == id;
              });

              // Change HTML if we have this character alredy booked
              var button_text = (bookmarked_character.length > 0 )? 'Booked': 'Bookmark',
              button_class = (bookmarked_character.length > 0 )? 'btn-green': 'btn-red',
              on_click = (bookmarked_character.length > 0 )? '': 'app.bookmark(this)';

              // Generate search result HTML
              result_html +=
              '<div class="marvel-character">\
              <div class="marvel-character__inner">\
              <div class="marvel-character__image" style="background-image:url(' + thumbnail.path + '/standard_fantastic.' + thumbnail.extension + ');"></div>'+
              '<h4 class="marvel-character__name">' + name +'</h4>' +
              '<button class="marvel-character__bookmark-button btn ' + button_class + '" id="' + id + '" onclick="' + on_click + '">' + button_text + '</button>\
              </div>\
              </div>';
            }

            // No results found
          }else{
            result_html += '<h2 class="text-center">No characters found.</h2>'
          }

          // Append results to DOM
          app.marvel_results.innerHTML = result_html;

          // Clear DOm of pagination links and load new with prev/next and what to paginate condition
          // Remove pagination links
          app.remove_pagination();
          var prev_page = ( app.offset == 0 )? false: true;

          if( results.length == app.limit  ){
            var next_page = true
            app.pagination(prev_page, next_page, 'search-results');
          }else{
            var next_page = false
            app.pagination(prev_page, next_page, 'search-results');
          }

        });

        // If search field is empty list bookmarks
      }else{
        app.list_bookmarks();
      }
    }, app.timeout_delay);

    // Set timeout delay to default 500ms
    app.timeout_delay = 500;
  },


  ///
  // List bookmarks on frontend
  ///
  list_bookmarks: function(){

    // Clean previous pagination links
    app.remove_pagination();

    // Get bookmarked characters
    var bookmarked_characters = JSON.parse(localStorage.getItem('___bookmarked_characters'));
    bookmarked_characters = (bookmarked_characters == null)? [] : bookmarked_characters,
    result_html = '';

    // If we have bookmarked characters
    if(bookmarked_characters.length > 0){

      // Loop true avaliable bookmarsk and show results based on pagination offset
      for (var i = (app.offset-1 <= 0)? 0: app.offset; i < (app.offset + app.limit)-1; i++) {
        if(typeof bookmarked_characters[i] != 'undefined'){
          var name = bookmarked_characters[i].name,
          thumbnail = bookmarked_characters[i].thumbnail,
          id = bookmarked_characters[i].id;

          // Generate booking result HTML
          result_html +=
          '<div class="marvel-character">\
          <div class="marvel-character__inner">\
          <div class="marvel-character__image" style="background-image:url(' + thumbnail + ');"></div>'+
          '<h4 class="marvel-character__name">' + name +'</h4>' +
          '<button class="marvel-character__bookmark-button btn btn-green pointer" id="' + id + '" onclick="app.remove_bookmark(this)">Remove booking</button>\
          </div>\
          </div>';
        }
      }

      // Clear DOM of pagination links and load new with prev/next and what to paginate condition
      // Remove pagination links
      app.remove_pagination();
      var prev_page = ( app.offset == 0 )? false: true;
      if( bookmarked_characters.length > (app.offset+(app.limit-1)) ){
        var next_page = true
        app.pagination(prev_page, next_page, 'bookmarks');
      }else{
        var next_page = false
        app.pagination(prev_page, next_page, 'bookmarks');
      }

    // If no bookmarked characters
    }else{
        result_html += '<h2 class="text-center">No characters booked.</h2>';
    }

    // Append results to DOM
    app.marvel_results.innerHTML = result_html;

  },


  ///
  // Define app pagination function
  ///
  pagination: function(prev_page, next_page, pagination_target){

    // Change HTML if we have prev and next page.
    var on_click_next = (next_page)? "app.switch_page(this,'"+pagination_target+"')": '',
    on_click_prev = (prev_page)? "app.switch_page(this,'"+pagination_target+"')": '',
    prev_button_class = (prev_page)?' btn-blue':' disabled',
    next_button_class = (next_page)?' btn-blue':' disabled';

    // Define pagination HTML.
    var pagination_html = '<div class="pagination">\
      <button class="pagination__button btn' + prev_button_class + '" id="prev" onclick="' + on_click_prev + '">Prev</button>\
      <span class="pagination__page">Page (' + app.page_number + ')</span>\
      <button class="pagination__button pagination_next btn'+next_button_class+'" id="next" onclick="' + on_click_next + '">Next</button>\
    </div>';

    // Show pagination links only if we have enough results to paginate
    if(prev_page || next_page){
      this.marvel_results.insertAdjacentHTML('afterend', pagination_html);
    }
  },


  ///
  // Switch to new page when clicked on pagination buttons
  ///
  switch_page: function(element, pagination_target){

    // Disable element to prevent spam and get pagination direction stored in id
    element.disabled = true;
    var id = element.id,
    page_number = element.parentNode.querySelector('.pagination__page').innerText;

    // If we click on next or prev button
    // Update offset and page number
    if(id == 'next'){
      app.page_number = app.page_number + 1;
      app.offset = app.offset + (app.limit-1);

    }else{
      app.page_number = app.page_number - 1;
      app.offset = app.offset - (app.limit-1);
    }

    // Set keyup delay to 0
    app.timeout_delay = 0;

    // Simulate keyup or list bookmarks to rerender results
    if(pagination_target == 'search-results'){
      app.simulateKeyup( this.search_field );
    }else{
      app.list_bookmarks();
    }

  },


  ///
  // Remove pagination, used to clear DOM
  ///
  remove_pagination: function(){
    var pagination = document.querySelector('.pagination');
    if(pagination != null){
      pagination.remove();
    }
  },


  ///
  //  Bookmark character
  //
  bookmark: function(element){

    // Get bookmarks character data
    var character_id = element.id;
    var character_background_image = element.parentNode.querySelector('.marvel-character__image').style.backgroundImage;
    character_background_image = character_background_image.replace('url(','').replace(')','').replace('"',''),
    character_name = element.parentNode.querySelector('.marvel-character__name').innerText;

    // Get all bookmarks
    bookmarked_characters = JSON.parse(localStorage.getItem('___bookmarked_characters'));

    // If we dont have bookmarks update bookmark localstorage with new character.
    if(bookmarked_characters == null){
        localStorage.setItem('___bookmarked_characters', JSON.stringify([{id:character_id, name:character_name, thumbnail:character_background_image}]));

    // If we alredy have bookmarks
    }else{

      // Push new characted to bookmark array, then update localstorage bookmarks
      bookmarked_characters.push({id:character_id, name:character_name, thumbnail: character_background_image});
      localStorage.setItem('___bookmarked_characters', JSON.stringify(bookmarked_characters));
    }

    // Update DOM with different looking button and detach click event
    element.innerHTML = 'Booked';
    app.removeClass(element, 'btn-red').addClass(element, 'btn-green').removeEvent(element, 'click', app.bookmark);
    element.removeAttribute('onclick');
  },


  ///
  // Remove bookmarks
  ///
  remove_bookmark: function(element){

    // Get id of character to remove
    var id = element.id,

    // Get all bookmarks and remove character with current id
    bookmarked_characters = JSON.parse(localStorage.getItem('___bookmarked_characters'));
    bookmarked_characters = bookmarked_characters.filter(function(e){
      return e.id != id;
    });

    // If bookmarks array is empty remove localstorage
    if( bookmarked_characters.length == 0 ){
      localStorage.removeItem('___bookmarked_characters');

    // Update bookmarks
    }else{
      localStorage.setItem('___bookmarked_characters', JSON.stringify(bookmarked_characters));
    }

    // Update offset and page number when removing elements
    if(Number.isInteger(bookmarked_characters.length/(app.limit-1))){
      app.offset = app.offset - (app.limit-1);
      app.page_number = app.page_number - 1;

      app.offset = (app.offset<=0)? 0:app.offset;
      app.page_number = (app.page_number<=0)? 1: app.page_number;
    }

    // Rerender bookmarks
    app.list_bookmarks();
  },


  ///
  // Helper functions
  ///

  // Simulate keyup event
  simulateKeyup :function (elem) {
  	// Create our event (with options)
  	var evt = new MouseEvent('keyup', {
  		bubbles: true,
  		cancelable: true,
  		view: window
  	});
  	// If cancelled, don't dispatch our event
  	var canceled = !elem.dispatchEvent(evt);
  },

  // Add class to element
  addClass: function(el, className){
    if (el.classList){
        el.classList.add(className);
    }else if (!hasClass(el, className)){
        el.className += " " + className;
    }
    return app;
  },

  // Remove class from element
  removeClass: function(el, className){
    if (el.classList){
        el.classList.remove(className);
    }else if (hasClass(el, className)){
        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
        el.className = el.className.replace(reg, ' ');
    }
    return app;
  },

  // Remove event from element
  removeEvent: function(el, type, handler) {
    if (el.detachEvent) el.detachEvent('on'+type, handler); else el.removeEventListener(type, handler);
    return app;
  }
}


///
// Initialize our App
///
app.init();
